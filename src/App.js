import Providers from "./providers";
import Routes from "./routes";
import GlobalStyles from "./styles/global";

function App() {
  return (
    <>
      <Providers>
        <GlobalStyles />
        <Routes />
      </Providers>
    </>
  );
}

export default App;
