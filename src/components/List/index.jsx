import { Button } from "../ProductCard/styles";
import { Container, ImageWrapper, ListItem } from "./styles";

const List = ({ products, setProducts, event }) => {
  return (
    <>
      <Container>
        <h2>
          {event} {products.length === 0 && <span>no products added</span>}
        </h2>
        {products &&
          products.map((item, index) => (
            <ListItem key={index}>
              <ImageWrapper>
                <img src={item.image_url} alt="" />
              </ImageWrapper>
              <h3>
                <span>Item: </span>
                {item.name}
              </h3>
              <Button onClick={() => setProducts(item)}>remove</Button>
            </ListItem>
          ))}
      </Container>
    </>
  );
};

export default List;
