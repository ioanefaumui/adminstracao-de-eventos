import styled from "styled-components";

export const Container = styled.div`
  max-width: 1300px;
  margin: 0 auto 132px auto;

  h2 {
    margin-top: 32px;
  }

  span {
    color: #c4c4c4;
  }
`;

export const ListItem = styled.div`
  margin-top: 32px;
  height: 100px;
  display: flex;
  align-items: center;
  gap: 32px;
  background-color: #ececec;
`;

export const ImageWrapper = styled.div`
  width: 100px;
  height: 100%;
  display: flex;
  background-color: #c4c4c4;
  justify-content: center;
  align-items: center;

  img {
    height: 80%;
  }
`;
