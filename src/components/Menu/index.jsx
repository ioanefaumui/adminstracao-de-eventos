import { Link } from "react-router-dom";
import { Background } from "./styles";
import { MdArrowDropUp } from "react-icons/md";

const Menu = () => {
  return (
    <Background>
      <div className="menu-wrapper">
        <div className="menu">
          <button to="/" className="events">
            Events{" "}
            <span>
              <MdArrowDropUp className="icon" />
            </span>
            <div className="submenu">
              <Link to="/get_together">Get-together</Link>
              <Link to="/graduation">Graduation</Link>
              <Link to="/wedding">Wedding</Link>
            </div>
          </button>
          <Link to="/">Products</Link>
          <button>Logout</button>
        </div>
      </div>
    </Background>
  );
};

export default Menu;
