import styled from "styled-components";

export const Background = styled.div`
  height: 100px;
  width: 100%;
  background: linear-gradient(rgba(255, 255, 255, 0), black);
  position: fixed;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;

  .menu-wrapper {
    position: relative;
  }

  .menu {
    display: flex;
    background-color: #ececec;
    border-radius: 8px;
  }

  .icon {
    position: absolute;
    left: 105px;
    bottom: 11px;
  }

  .submenu {
    transition: ease-in 200ms;
    display: flex;
    width: 100%;
    flex-direction: column;
    position: absolute;
    background-color: #ececec;
    transform: translate(-40px, -32px);
    border-radius: 8px;
    height: 0;
    overflow: hidden;
    text-align: left;
  }

  .events:hover > .submenu {
    height: 300%;
    transition: ease-out 200ms;
  }

  .events {
    display: flex;
    flex-direction: column-reverse;
    padding-right: 48px;
  }

  a,
  button {
    padding: 8px 40px;
    border-radius: 8px;
    font-size: 1.2rem;

    :hover {
      background-color: #c4c4c4;
      transition: ease 200ms;
    }
  }
`;
