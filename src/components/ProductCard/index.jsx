import {
  Container,
  Button,
  CardWrapper,
  ContentWrapper,
  ImageWrapper,
  SelectWrapper,
} from "./styles";

import { useEffect, useState } from "react";
import { useProducts } from "../../providers/products";
import Menu from "../Menu";
import Loading from "../Loading";
import { useGetTogether } from "../../providers/getTogether";
import { useGraduation } from "../../providers/graduation";
import { useWedding } from "../../providers/wedding";

const ProductCard = () => {
  const [lookProducts, setLookProducts] = useState([]);
  const [load, setLoad] = useState(false);
  const [content, setContent] = useState(false);
  const { products } = useProducts();
  const { addToGetTogether } = useGetTogether();
  const { addToGraduation } = useGraduation();
  const { addToWedding } = useWedding();

  const [event, setEvent] = useState("");

  const handleAddProduct = (item) => {
    if (event === "get-together") {
      addToGetTogether(item);
    }
    if (event === "graduation") {
      addToGraduation(item);
    }
    if (event === "wedding") {
      addToWedding(item);
    }
  };

  useEffect(() => {
    setTimeout(() => setLoad(true), 1900);
    setTimeout(() => setContent(true), 2000);
  }, []);

  useEffect(() => {
    setLookProducts(products);
  }, [products]);

  return (
    <>
      {!load && <Loading />}

      <Container
        style={
          content === true ? { opacity: 100, height: "auto" } : { opacity: 0 }
        }
      >
        {lookProducts &&
          lookProducts.map((product, index) => (
            <CardWrapper key={index}>
              <ImageWrapper>
                <img src={product.image_url} alt="" />
              </ImageWrapper>
              <ContentWrapper>
                <h3>{product.name}</h3>
                <p>
                  <span>First brewed: </span>
                  {product.first_brewed}
                  <br />
                </p>
                <p>
                  <span>Description: </span>
                  {product.description}
                </p>

                <SelectWrapper>
                  <span>Event: </span>
                  <select
                    name="Event"
                    onChange={(e) => setEvent(e.target.value)}
                  >
                    <option value="" hidden>
                      Please select
                    </option>
                    <option value="get-together">Get-together</option>
                    <option value="graduation">Graduation</option>
                    <option value="wedding">Wedding</option>
                  </select>
                </SelectWrapper>

                <Button onClick={() => handleAddProduct(product)}>
                  Add product
                </Button>
              </ContentWrapper>
            </CardWrapper>
          ))}
        <Menu />
      </Container>
    </>
  );
};

export default ProductCard;
