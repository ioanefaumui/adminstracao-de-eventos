import styled, { css } from "styled-components";

export const Container = styled.div`
  padding: 32px 0 132px 0;
  max-width: 1300px;
  margin: 0 auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  height: 100vh;
  overflow: hidden;
  gap: 32px;
  opacity: 0;
  transition: opacity ease-in 500ms;
`;

export const CardWrapper = styled.div`
  display: flex;
  height: 300px;
  width: calc(1235px / 3);
  background: #c4c4c4;
  select {
    padding: 8px 0;
    width: 100%;
  }
`;

export const ImageWrapper = styled.div`
  height: 100%;
  width: 40%;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    height: 80%;
  }
`;

export const ContentWrapper = styled.div`
  gap: 16px;
  display: flex;
  flex-direction: column;
  padding: 16px;
  width: 60%;
  height: 100%;
  background-color: #ececec;

  h3 {
    width: calc(100% - 8px);
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  p:nth-child(3) {
    height: 84px;
    overflow: hidden;
    text-overflow: ellipsis;
    color: rgba(0, 0, 0, 0.7);
  }

  span {
    color: rgba(0, 0, 0, 0.35);
  }
`;

export const SelectWrapper = styled.div`
  align-items: center;
  gap: 4px;
  display: flex;
  white-space: nowrap;
`;

export const Button = styled.button`
  color: snow;
  padding: 8px 16px;
  letter-spacing: 1px;
  background-color: rgb(0, 0, 0);
  transition: filter ease 200ms;

  :hover {
    filter: brightness(0.9);
  }

  ${(props) =>
    props.remove &&
    css`
      background-color: crimson;
    `}
`;
