import List from "../../components/List";
import Menu from "../../components/Menu";
import { useGetTogether } from "../../providers/getTogether";

const GetTogether = () => {
  const { getTogether, removeFromGetTogether } = useGetTogether();

  return (
    <>
      <List
        products={getTogether}
        setProducts={removeFromGetTogether}
        event="Get-together: "
      />
      <Menu />
    </>
  );
};

export default GetTogether;
