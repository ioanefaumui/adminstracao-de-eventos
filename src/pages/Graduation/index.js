import List from "../../components/List";
import Menu from "../../components/Menu";
import { useGraduation } from "../../providers/graduation";

const Graduation = () => {
  const { graduation, removeFromGraduation } = useGraduation();

  return (
    <>
      <List
        products={graduation}
        setProducts={removeFromGraduation}
        event="Graduation: "
      />
      <Menu />
    </>
  );
};

export default Graduation;
