import List from "../../components/List";
import Menu from "../../components/Menu";
import { useWedding } from "../../providers/wedding";

const Wedding = () => {
  const { wedding, removeFromWedding } = useWedding();

  return (
    <>
      <List
        products={wedding}
        setProducts={removeFromWedding}
        event="Wedding: "
      />
      <Menu />
    </>
  );
};

export default Wedding;
