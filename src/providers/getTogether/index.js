import { createContext, useContext, useEffect, useState } from "react";

const GetTogetherContext = createContext();

export const GetTogetherProvider = ({ children }) => {
  const [getTogether, setGetTogether] = useState(() => {
    const localGetTogether =
      JSON.parse(localStorage.getItem("getTogether")) || [];
    return localGetTogether;
  });

  const addToGetTogether = (item) => {
    setGetTogether([...getTogether, item]);
  };

  useEffect(() => {
    localStorage.setItem("getTogether", JSON.stringify(getTogether));
  }, [getTogether]);

  const removeFromGetTogether = (item) => {
    const newGetTogether = getTogether.filter(
      (contextItem) => contextItem.id !== item.id
    );
    setGetTogether(newGetTogether);
    localStorage.setItem("getTogether", JSON.stringify(newGetTogether));
  };

  return (
    <GetTogetherContext.Provider
      value={{ getTogether, addToGetTogether, removeFromGetTogether }}
    >
      {children}
    </GetTogetherContext.Provider>
  );
};

export const useGetTogether = () => useContext(GetTogetherContext);
