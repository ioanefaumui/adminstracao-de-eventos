import { createContext, useContext, useState, useEffect } from "react";

const GraduationContext = createContext();

export const GraduationProvider = ({ children }) => {
  const [graduation, setGraduation] = useState(() => {
    const localGraduation =
      JSON.parse(localStorage.getItem("graduation")) || [];
    return localGraduation;
  });

  const addToGraduation = (item) => {
    setGraduation([...graduation, item]);
  };

  useEffect(() => {
    localStorage.setItem("graduation", JSON.stringify(graduation));
  }, [graduation]);

  const removeFromGraduation = (item) => {
    const newGraduation = graduation.filter(
      (contextItem) => contextItem.id !== item.id
    );
    setGraduation(newGraduation);
    localStorage.setItem("graduation", JSON.stringify(newGraduation));
  };

  return (
    <GraduationContext.Provider
      value={{ graduation, addToGraduation, removeFromGraduation }}
    >
      {children}
    </GraduationContext.Provider>
  );
};

export const useGraduation = () => useContext(GraduationContext);
