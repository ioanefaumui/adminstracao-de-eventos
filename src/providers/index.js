import { GetTogetherProvider } from "./getTogether";
import { GraduationProvider } from "./graduation";
import { ProductsProvider } from "./products";
import { WeddingProvider } from "./wedding";

const Providers = ({ children }) => {
  return (
    <ProductsProvider>
      <GetTogetherProvider>
        <GraduationProvider>
          <WeddingProvider>{children}</WeddingProvider>
        </GraduationProvider>
      </GetTogetherProvider>
    </ProductsProvider>
  );
};

export default Providers;
