import { createContext, useContext, useEffect, useState } from "react";

import axios from "axios";

const ProductsContext = createContext();

export const ProductsProvider = ({ children }) => {
  const [products, setProducts] = useState(() => {
    const getProducts = JSON.parse(localStorage.getItem("products")) || [];
    return getProducts;
  });

  const getProducts = async () => {
    if (products.length === 0) {
      console.log("here");
      await axios
        .get("https://api.punkapi.com/v2/beers?page=1&per_page=21")

        .then((response) =>
          localStorage.setItem("products", JSON.stringify(response.data))
        );

      await setProducts(JSON.parse(localStorage.getItem("products")));
    }
  };

  useEffect(() => {
    getProducts();
  });

  return (
    <ProductsContext.Provider value={{ products, setProducts }}>
      {children}
    </ProductsContext.Provider>
  );
};

export const useProducts = () => useContext(ProductsContext);
