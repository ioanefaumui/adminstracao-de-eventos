import { createContext, useContext, useEffect, useState } from "react";

const WeddingContext = createContext();

export const WeddingProvider = ({ children }) => {
  const [wedding, setWedding] = useState(() => {
    const localWedding = JSON.parse(localStorage.getItem("wedding")) || [];
    return localWedding;
  });

  const addToWedding = (item) => {
    setWedding([...wedding, item]);
  };

  useEffect(() => {
    localStorage.setItem("wedding", JSON.stringify(wedding));
  });

  const removeFromWedding = (item) => {
    const newWedding = wedding.filter(
      (contextItem) => contextItem.id !== item.id
    );
    setWedding(newWedding);
    localStorage.setItem("wedding", JSON.stringify(newWedding));
  };

  return (
    <WeddingContext.Provider
      value={{ wedding, addToWedding, removeFromWedding }}
    >
      {children}
    </WeddingContext.Provider>
  );
};

export const useWedding = () => useContext(WeddingContext);
