import { Route, Switch } from "react-router";
import Dashboard from "../pages/Dashboard";
import GetTogether from "../pages/GetTogether";
import Graduation from "../pages/Graduation";
import Wedding from "../pages/Wedding";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Dashboard />
      </Route>
      <Route path="/get_together">
        <GetTogether />
      </Route>
      <Route path="/graduation">
        <Graduation />
      </Route>
      <Route path="/wedding">
        <Wedding />
      </Route>
    </Switch>
  );
};

export default Routes;
