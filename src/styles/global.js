import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        outline: 0;
        border: none;
        text-decoration: none;
        box-sizing: border-box;
    }

    body, input, button, select{
        font-size: 1rem;
        font-family: 'Rajdhani', sans-serif;
    }

    body{
        background-color: snow;
    }

    h1, h2, h3, h4, h5, h6 {
        font-weight: 700;
        font-family: 'Play', sans-serif;
    }

    button{
        cursor: pointer;
    }

    a {
        color: black;
    }
`;
